﻿#pragma strict
private var indexes : Array;
private var partsInGame : Array = new Array();
private var numbersOfParts = [8, 8, 8, 8, 8, 8, 15, 15, 15, 15, 15, 15, 24, 24, 24, 24, 24, 24];
private var musicPlay : boolean = true;
private var zoom : boolean = true;
private var dxy : float = 0;
private var x : float = -1.75;
private var y : float = 1.5;
private var prt : GameObject;
private var adCount : int = 0;
private var currentDifficulty : int = 1;
private var currentLevel : int = 1;
private var currentTheme : int = 1;
private var i : int = 0;
private var parts2D : Part[,];
private var parts : Sprite[,];
private var unlockedDifficulties : String;
private var completedLevels : String[];

//public var music : AudioClip;
public var pageNumber: int = 0;
public var a : GameObject;
public var backGallery : GameObject;
public var easyPanel : GameObject;
public var firstPanel : GameObject;
public var galleryPanel : GameObject;
public var gameOverPanel : GameObject;
public var hardPanel : GameObject;
public var mediumPanel : GameObject;
public var miniPanel : GameObject;
public var optionsPanel : GameObject;
public var pages : GameObject[];
public var part : GameObject;
public var playPanel : GameObject;
public var backGroundSprites : Sprite[];
public var musicSprites : Sprite[];
public var partsInOneArray : Sprite[];

public static var instance : GC;

private function Start () {
	if(instance == null) {
		instance = this;
	} else if(instance != this) {
		Destroy(gameObject);
	}
	completedLevels = new String[3];
	for(var j = 0; j < 3; j++) {		
		if(PlayerPrefs.HasKey("cl" + (j+1).ToString())) {			
			completedLevels[j] = PlayerPrefs.GetString("cl" + (j+1).ToString());
		} else {
			completedLevels[j] = "000000";
		}
	}
	unlockedDifficulties = PlayerPrefs.HasKey("ud") ? PlayerPrefs.GetString("ud") : "100";
	OneArrayToParts();
	if(PlayerPrefs.HasKey("music")){
		musicPlay = PlayerPrefs.GetInt("music") == 1;
		if(musicPlay){
				GetComponent(AudioSource).Play();
			} else {
				GetComponent(AudioSource).Pause();
			}
			firstPanel.transform.GetChild(2).gameObject.GetComponent(SpriteRenderer).sprite = musicSprites[musicPlay ? 0 : 1];
			miniPanel.transform.GetChild(5).gameObject.GetComponent(SpriteRenderer).sprite = musicSprites[musicPlay ? 2 : 3];
	}
}
private function CanBeSolved(size : int) : boolean {
	var partsNumbersArray : int[] = new int[size * size - 1];
	var j : int = 0;
	for(var part in parts2D) {
		if(part == null) {
			continue;
		}
		partsNumbersArray[j] = part.GetFinalPlace().NumberInOrder(size);
		j++;
	}
	return InversionNumber(partsNumbersArray) % 2 == 0;
}

private function CreateIntArray(n : int) {
	indexes = [];//new Array();
	for(var i = 0; i < n; i++) {
		indexes.push(i);
	}
}

public function CreateLevel() {
	//var r = 0;
	do {
		GenerateLevel();
	} while(!CanBeSolved(Mathf.Sqrt(numbersOfParts[i] + 1)));
	var xStart : float = 0;
	var yStart : float = 0;
	switch(currentDifficulty){
		case 1:
			dxy = 1.7025;
			xStart = -1.7025;
			yStart = 1.5;
			break;
		case 2:
			dxy = 1.275;
			xStart = -1.91625; //-1.9637;
			yStart = 1.71375; //2.2137
			break;
		case 3:
			dxy = 1.02;
			xStart = -2.04375; //-2.09125;
			yStart = 1.84125;//2.34125;
	}
	x = xStart;
	y = yStart;
	var levelPack : GameObject = new GameObject();
	levelPack.transform.name = "Level";
	//CreateIntArray(numbersOfParts[i]);
	//parts2D = new Part[Mathf.Sqrt(numbersOfParts[i] + 1),Mathf.Sqrt(numbersOfParts[i] + 1)];
	//for(var j = 0; j < numbersOfParts[i]; j++) {
	for(var j = 0; j < numbersOfParts[i]; j++) {
		prt = Instantiate(part, new Vector3(x, y, 0), Quaternion.identity);
		x += dxy;// 1.104
		if(j % Mathf.Sqrt(numbersOfParts[i] + 1) == Mathf.Sqrt(numbersOfParts[i] + 1) - 1) {
			y -= dxy;//1.104
			x = xStart;//-2
		}
		prt.transform.parent = levelPack.transform;
		//r = Random.Range(0, indexes.length - 1); 
		//Debug.Log(r);
		//var ir : int = indexes[r];
		prt.GetComponent(SpriteRenderer).sprite = parts[i, parts2D[j / Mathf.FloorToInt(Mathf.Sqrt(numbersOfParts[i] + 1)), 
			j % Mathf.Sqrt(numbersOfParts[i] + 1)].GetFinalPlace().NumberInOrder(Mathf.Sqrt(numbersOfParts[i] + 1)) - 1];
		prt.AddComponent(BoxCollider2D);
		parts2D[j / Mathf.FloorToInt(Mathf.Sqrt(numbersOfParts[i] + 1)), j % Mathf.Sqrt(numbersOfParts[i] + 1)].SetObject(prt);
		/*partsInGame.push(new Part(prt, j / Mathf.FloorToInt(Mathf.Sqrt(numbersOfParts[i] + 1)), j % Mathf.Sqrt(numbersOfParts[i] + 1), //current
			ir / Mathf.FloorToInt(Mathf.Sqrt(numbersOfParts[i] + 1)), ir % Mathf.Sqrt(numbersOfParts[i] + 1))); //final*/
		/*parts2D[j / Mathf.FloorToInt(Mathf.Sqrt(numbersOfParts[i] + 1)), j % Mathf.Sqrt(numbersOfParts[i] + 1)] = new Part(prt, 
		    j / Mathf.FloorToInt(Mathf.Sqrt(numbersOfParts[i] + 1)), j % Mathf.Sqrt(numbersOfParts[i] + 1), //current
			ir / Mathf.FloorToInt(Mathf.Sqrt(numbersOfParts[i] + 1)), ir % Mathf.Sqrt(numbersOfParts[i] + 1));//final
		indexes.splice(r, 1);*/
	}
	miniPanel.transform.GetChild(0).gameObject.GetComponent(SpriteRenderer).sprite = backGroundSprites[Random.Range(0 , backGroundSprites.length)];
}

private function GenerateLevel() {
	var r = 0;
	CreateIntArray(numbersOfParts[i]);
	parts2D = new Part[Mathf.Sqrt(numbersOfParts[i] + 1),Mathf.Sqrt(numbersOfParts[i] + 1)];
	for(var j = 0; j < numbersOfParts[i]; j++) {
		r = Random.Range(0, indexes.length);
		var ir : int = indexes[r];
		parts2D[j / Mathf.FloorToInt(Mathf.Sqrt(numbersOfParts[i] + 1)), j % Mathf.Sqrt(numbersOfParts[i] + 1)] = new Part(prt, 
		    j / Mathf.FloorToInt(Mathf.Sqrt(numbersOfParts[i] + 1)), j % Mathf.Sqrt(numbersOfParts[i] + 1), //current
			ir / Mathf.FloorToInt(Mathf.Sqrt(numbersOfParts[i] + 1)), ir % Mathf.Sqrt(numbersOfParts[i] + 1));//final
		indexes.splice(r, 1);
	}
}

private function InversionNumber(a : int[]) : int {
	var inversions = 0;
	for(var j = 0; j < a.length; j++) {
		for(var k = j + 1; k < a.length; k++) {
			if(a[k] < a[j]) {
				inversions++;
			}
		}
	}
	return inversions;
}

public function GetPart1D(i : int) : Part {
	var q : Part[] = partsInGame.ToBuiltin(Part) as Part[];
	return q[i];
}

public function GetPartByObject1D(o : GameObject) : Part {
	var q : Part[] = partsInGame.ToBuiltin(Part) as Part[];
	for(var p in q) {
		if(p.GetObject() == o) {
			//var ind = partsInOneArray.IndexOf(p);
			return p;
		}
	}
	return null;
}

public function GetPart(i : int, j : int) : Part {
	return parts2D[i,j];
}

public function GetPartByObject(o : GameObject) : Part {
	for(var p = 0; p < Mathf.FloorToInt(Mathf.Sqrt(parts2D.length)); p++) {
		for(var q = 0; q < Mathf.FloorToInt(Mathf.Sqrt(parts2D.length)); q++) {
			if(parts2D[p,q] == null) {
				continue;
			}
			if(parts2D[p,q].GetObject() == o) {
				return parts2D[p,q];
			}
		}
	}
	return null;
}

public function Level(o : Transform){
	currentTheme = int.Parse(o.name[1].ToString());
	currentDifficulty = int.Parse(o.name[2].ToString());
	currentLevel = int.Parse(o.name[3].ToString());
	i = 18 * (currentTheme - 1) + 6 * (currentDifficulty - 1) + currentLevel - 1;
	miniPanel.SetActive(true);
	easyPanel.SetActive(false);
	mediumPanel.SetActive(false);
	hardPanel.SetActive(false);
	CreateLevel();
} 

private function OneArrayToParts() {
	var n = 0;
	parts = new Sprite[numbersOfParts.length, 25];
	for(var i = 0; i < numbersOfParts.length; i++) {
		for(var j = 0; j < numbersOfParts[i]; j++) {
			parts[i,j] = partsInOneArray[n];
			n++;
		}
	}
}

private function ChangeSymbolInTheString(s : String, n : int, newSymbol : String) : String{
	var s1 : String = "";
	for(var j = 0; j < s.length; j++) {
		s1 += j == n ? newSymbol : s[j];
	}
	return s1;
}

/*private function ClearLevel() {
	Destroy(GameObject.Find("Level"));
	x = -1.75;
	y = 2.0;
}*/

private function HasZero(s : String) {
	for(var e in s) {
		if(e == "0") {
			return true;
		}
	}
	return false;
}

public function CheckVictory() : boolean {
	for(var p = 0; p < Mathf.FloorToInt(Mathf.Sqrt(parts2D.length)); p++) {
		for(var q = 0; q < Mathf.FloorToInt(Mathf.Sqrt(parts2D.length)); q++) {
			if(parts2D[p,q] == null) {
				continue;
			}
			if(!parts2D[p,q].IsInTheRightPlace()) {
				return false;
			}
		}
	}
	return true;
}

public function ClearLevel() {
	Destroy(GameObject.Find("Level"));
	x = -1.75;
	y = 2.0;
}

public function GameOver() {
	gameOverPanel.SetActive(true);
	var hasZero : boolean = HasZero(completedLevels[currentDifficulty - 1]);
	completedLevels[currentDifficulty - 1] = ChangeSymbolInTheString(completedLevels[currentDifficulty - 1], currentLevel - 1, "1");
	PlayerPrefs.SetString("cl" + currentDifficulty.ToString(), completedLevels[currentDifficulty - 1]);
	if(hasZero && completedLevels[currentDifficulty - 1] == "111111") {
		//unlocked next difficulty
		unlockedDifficulties = currentDifficulty == 3 ? unlockedDifficulties : ChangeSymbolInTheString(unlockedDifficulties, currentDifficulty, "1");
		PlayerPrefs.SetString("ud", unlockedDifficulties);
		gameOverPanel.transform.GetChild(currentDifficulty + 2).gameObject.SetActive(true);
	} else {
		gameOverPanel.transform.GetChild(2).gameObject.SetActive(true);
	}
	if(currentLevel == 6 && (currentDifficulty == 3 || completedLevels[currentDifficulty - 1] != "111111")){		
		gameOverPanel.transform.GetChild(0).gameObject.SetActive(false);

	}
	adCount ++;
	if(adCount % 3 == 1) {
		a.transform.parent = transform;
	}
	ClearLevel();
}

public function GetCompletedLevels() : String[] {
	return completedLevels;
}
public function GetDxy() : float{
	return dxy;
}
public function GetParts2D() : Part[,] {
	return parts2D;
}
public function GetNumberOfParts() : int[] {
	return numbersOfParts;
}
public function GetI() : int {
	return i;
}
public function GetUnlockedDifficulties() : String{
	return unlockedDifficulties;
}
public function GetMusicPlay() : boolean{
	return musicPlay;
}
public function SetMusicPlay(musicPlay : boolean){
	this.musicPlay = musicPlay;
}
public function GetZoom() : boolean{
	return zoom;
}
public function SetZoom(zoom : boolean){
	this.zoom = zoom;
}
public function Move(s : Place, f : Place) {
	parts2D[f.GetRow(), f.GetCol()] = parts2D[s.GetRow(), s.GetCol()];
	parts2D[s.GetRow(), s.GetCol()] = null;
	parts2D[f.GetRow(), f.GetCol()].SetCurrentPlace(f);
}

public function NextLevel() {
	currentLevel++;
	if(currentLevel > 6) {
		currentDifficulty++;
		currentLevel = 1;
	}
	if(currentDifficulty > 3) {
		currentDifficulty = 1;
		currentTheme++;
	}
    i = 18 * (currentTheme - 1) + 6 * (currentDifficulty - 1) + currentLevel - 1;
    CreateLevel();
}

public class Place {
	private var row : int;
	private var col : int;

	public function Place(row : int, col : int) {
		this.row = row;
		this.col = col;
	}

	public function GetRow() : int {
		return row;
	}
	public function SetRow(row : int) {
		this.row = row;
	}
	public function GetCol() : int {
		return col;
	}
	public function SetCol(col : int) {
		this.col = col;
	}

	public function Equals(other : Place) : boolean {
		return (row == other.GetRow()) && (col == other.GetCol());
	}
	public function NumberInOrder(size : int) : int {
		return row * size + col + 1;
	}
}

public class Part {
	private var object : GameObject;
	private var currentPlace : Place;
	private var finalPlace : Place;

	public function Part(object : GameObject, cPr : int, cPc : int, fPr : int, fPc : int) {
		this.object = object;
		currentPlace = new Place(cPr, cPc);
		finalPlace = new Place(fPr, fPc);
	}

	public function GetObject() : GameObject {
		return object;
	}
	public function SetObject(object : GameObject) {
		this.object = object;
	}
	public function GetCurrentPlace() : Place {
		return currentPlace;
	}
	public function SetCurrentPlace(currentPlace : Place) {
		this.currentPlace = currentPlace;
	}
	public function GetFinalPlace() : Place {
		return finalPlace;
	}
	public function SetFinalPlace(currentPlace : Place) {
		this.finalPlace = currentPlace;
	}

	public function IsInTheRightPlace() : boolean {
		return currentPlace.Equals(finalPlace);
	}
}
