﻿#pragma strict

function OnMouseDown(){
	switch(transform.name){
		case "PlayButt" : 
			GC.instance.firstPanel.SetActive(false);
			GC.instance.playPanel.SetActive(true);
			GC.instance.playPanel.transform.GetChild(2).GetChild(0).gameObject.SetActive(GC.instance.GetUnlockedDifficulties()[1] == "0");
			GC.instance.playPanel.transform.GetChild(3).GetChild(0).gameObject.SetActive(GC.instance.GetUnlockedDifficulties()[2] == "0");
			break;		
		case "GalleryButton" :
			GC.instance.firstPanel.SetActive(false);
			GC.instance.galleryPanel.SetActive(true);
			for(var j1 = 1; j1 < 4; j1++){
				for(var i1 = 0; i1 < 6; i1++){
					GC.instance.galleryPanel.transform.GetChild(j1).GetChild(i1).GetChild(0).gameObject.SetActive(!(GC.instance.GetCompletedLevels()[j1-1][i1] == "1"));
					GC.instance.galleryPanel.transform.GetChild(j1).GetChild(i1).GetChild(1).gameObject.SetActive(GC.instance.GetCompletedLevels()[j1-1][i1] == "1");
				}
			}
			break;		
		case "EasyLvl" :
			GC.instance.playPanel.SetActive(false);
			GC.instance.easyPanel.SetActive(true);
			for(var i = 1; i < 7; i++){				
				GC.instance.easyPanel.transform.GetChild(i).GetChild(0).gameObject.SetActive(GC.instance.GetCompletedLevels()[0][i-1] == "1");
			}
			break;
		case "MediumLvl" :
			if(GC.instance.GetUnlockedDifficulties()[1] == "1"){
				GC.instance.playPanel.SetActive(false);
				GC.instance.mediumPanel.SetActive(true);
				for(var j = 1; j < 7; j++){					
					GC.instance.mediumPanel.transform.GetChild(j).GetChild(0).gameObject.SetActive(GC.instance.GetCompletedLevels()[1][j-1] == "1");
				}
			}
			break;
		case "HardLvl" :
			if(GC.instance.GetUnlockedDifficulties()[2] == "1"){
				GC.instance.playPanel.SetActive(false);
				GC.instance.hardPanel.SetActive(true);
				for(var v = 1; v < 7; v++){
					GC.instance.hardPanel.transform.GetChild(v).GetChild(0).gameObject.SetActive(GC.instance.GetCompletedLevels()[2][v-1] == "1");
				}
			}
			break;
		case "GoToFirstPanel" :
			GC.instance.firstPanel.SetActive(true);
			GC.instance.playPanel.SetActive(false);
			GC.instance.galleryPanel.SetActive(false);
			GC.instance.optionsPanel.SetActive(false);
			break;
		case "NextLevel" :
			GC.instance.NextLevel();
			GC.instance.gameOverPanel.SetActive(false);
			GC.instance.gameOverPanel.transform.GetChild(2).gameObject.SetActive(false);
			GC.instance.gameOverPanel.transform.GetChild(3).gameObject.SetActive(false);
			GC.instance.gameOverPanel.transform.GetChild(4).gameObject.SetActive(false);
			break;
		case "GoToDiffPanel" :
			GC.instance.playPanel.SetActive(true);
			GC.instance.easyPanel.SetActive(false);
			GC.instance.mediumPanel.SetActive(false);
			GC.instance.hardPanel.SetActive(false);
			break;
		case "Reset" :
			GC.instance.ClearLevel();
			GC.instance.CreateLevel();
			break;
		case "MainMenu" :
			GC.instance.firstPanel.SetActive(true);
			GC.instance.ClearLevel();
			GC.instance.miniPanel.SetActive(false);
			GC.instance.gameOverPanel.SetActive(false);
			break;
		case "NextButton" :
			GC.instance.pages[GC.instance.pageNumber % 3].SetActive(false);
			GC.instance.pageNumber ++;
			GC.instance.pages[GC.instance.pageNumber % 3].SetActive(true);
			break;
		case "PrevButton" :
			GC.instance.pages[GC.instance.pageNumber % 3].SetActive(false);
			GC.instance.pageNumber --;
			GC.instance.pageNumber += GC.instance.pageNumber < 0 ? 3 : 0;
			GC.instance.pages[GC.instance.pageNumber % 3].SetActive(true);
			break;
		case "Quit" :
			Application.Quit();
			break;
		case "OptionsButton" :
			GC.instance.SetMusicPlay(!GC.instance.GetMusicPlay());
			if(GC.instance.GetMusicPlay()){
				GC.instance.gameObject.GetComponent(AudioSource).Play();
			} else {
				GC.instance.gameObject.GetComponent(AudioSource).Pause();
			}
			PlayerPrefs.SetInt("music", GC.instance.GetMusicPlay() ? 1 : 0);
			GC.instance.firstPanel.transform.GetChild(2).gameObject.GetComponent(SpriteRenderer).sprite = GC.instance.musicSprites[GC.instance.GetMusicPlay() ? 0 : 1];
			GC.instance.miniPanel.transform.GetChild(5).gameObject.GetComponent(SpriteRenderer).sprite = GC.instance.musicSprites[GC.instance.GetMusicPlay() ? 2 : 3];
			break;		
	}

}