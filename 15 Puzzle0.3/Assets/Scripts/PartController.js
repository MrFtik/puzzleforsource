﻿#pragma strict
private var gameOver : boolean = false;
private var inMoving : boolean = false;

public var d : float = 1.7025;

private function Start() {
	d = GC.instance.GetDxy();
}

private function OnMouseDown() {
	if(GC.instance.GetPartByObject(this.gameObject).GetCurrentPlace().GetRow() > 0 && !inMoving) {//up
		if(GC.instance.GetParts2D()[GC.instance.GetPartByObject(this.gameObject).GetCurrentPlace().GetRow()-1,GC.instance.GetPartByObject(this.gameObject).GetCurrentPlace().GetCol()] 
			== null) {
			GC.instance.Move(GC.instance.GetPartByObject(this.gameObject).GetCurrentPlace(), 
				new Place(GC.instance.GetPartByObject(this.gameObject).GetCurrentPlace().GetRow()-1, GC.instance.GetPartByObject(this.gameObject).GetCurrentPlace().GetCol()));
			Invoke("CheckVictory", 1.5);
			// Moving with Lerp
			StartCoroutine(Moving(new Vector3(0,d,0)));
			return;
		}
	}
	if(GC.instance.GetPartByObject(this.gameObject).GetCurrentPlace().GetCol() < Mathf.Sqrt(GC.instance.GetNumberOfParts()[GC.instance.GetI()] + 1) - 1 && !inMoving) {//right
		if(GC.instance.GetParts2D()[GC.instance.GetPartByObject(this.gameObject).GetCurrentPlace().GetRow(),GC.instance.GetPartByObject(this.gameObject).GetCurrentPlace().GetCol()+1] 
			== null) {
			GC.instance.Move(GC.instance.GetPartByObject(this.gameObject).GetCurrentPlace(), 
				new Place(GC.instance.GetPartByObject(this.gameObject).GetCurrentPlace().GetRow(), GC.instance.GetPartByObject(this.gameObject).GetCurrentPlace().GetCol()+1));
			Invoke("CheckVictory", 1.5);
			// Moving with Lerp
			StartCoroutine(Moving(new Vector3(d,0,0)));
			return;
		}
	}
	if(GC.instance.GetPartByObject(this.gameObject).GetCurrentPlace().GetRow() < Mathf.Sqrt(GC.instance.GetNumberOfParts()[GC.instance.GetI()] + 1) - 1 && !inMoving) {//down
		if(GC.instance.GetParts2D()[GC.instance.GetPartByObject(this.gameObject).GetCurrentPlace().GetRow()+1,GC.instance.GetPartByObject(this.gameObject).GetCurrentPlace().GetCol()] 
			== null) {
			GC.instance.Move(GC.instance.GetPartByObject(this.gameObject).GetCurrentPlace(), 
				new Place(GC.instance.GetPartByObject(this.gameObject).GetCurrentPlace().GetRow()+1, GC.instance.GetPartByObject(this.gameObject).GetCurrentPlace().GetCol()));
			Invoke("CheckVictory", 1.5);
			// Moving with Lerp
			StartCoroutine(Moving(new Vector3(0,-d,0)));
			return;
		}
	}
	if(GC.instance.GetPartByObject(this.gameObject).GetCurrentPlace().GetCol() > 0 && !inMoving) {//left
		if(GC.instance.GetParts2D()[GC.instance.GetPartByObject(this.gameObject).GetCurrentPlace().GetRow(),GC.instance.GetPartByObject(this.gameObject).GetCurrentPlace().GetCol()-1] 
			== null) {
			GC.instance.Move(GC.instance.GetPartByObject(this.gameObject).GetCurrentPlace(), 
				new Place(GC.instance.GetPartByObject(this.gameObject).GetCurrentPlace().GetRow(), GC.instance.GetPartByObject(this.gameObject).GetCurrentPlace().GetCol()-1));
			Invoke("CheckVictory", 1.5);
			// Moving with Lerp
			StartCoroutine(Moving(new Vector3(-d,0,0)));
			return;
		}
	}
}

private function CheckVictory() {
	gameOver = GC.instance.CheckVictory();
	if(gameOver) {
		GC.instance.GameOver();
	}
}

public function Moving(dif : Vector3) : IEnumerator {
	inMoving = true;
	var start : Vector3 = transform.position;
	var finish : Vector3 = transform.position + dif;
	var t : float = 0.0;
	while(t <= 1.0) {
		t += Time.deltaTime * 5.0;
		yield WaitForSeconds(0.01);
		transform.position = Vector3.Lerp(start, finish, t);
	}
	inMoving = false;
	yield 0;
}
