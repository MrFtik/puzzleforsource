﻿using UnityEngine;
using System.Collections;
using AppodealAds.Unity.Api;
using AppodealAds.Unity.Common;
using System;

public class AM : MonoBehaviour
{
	#if UNITY_EDITOR && !UNITY_ANDROID && !UNITY_IPHONE
	string appKey = "";
	#elif UNITY_ANDROID
	string appKey = "09eea395c91da1af9b1adfdb86c25e43e92e80563d163357";
	/*#elif UNITY_IPHONE
     string appKey = "722fb56678445f72fe2ec58b2fa436688b920835405d3ca6";*/
	#else
	string appKey = "";
	#endif
	public GameObject miniPanel;
	public static AM instance;
	// Use this for initialization
	void Awake()
	{
		instance = this;
	}
	#if UNITY_ANDROID
	void Start()
	{
		Appodeal.disableWriteExternalStoragePermissionCheck();
		Appodeal.initialize(appKey, Appodeal.BANNER_BOTTOM | Appodeal.INTERSTITIAL);
	}

	// Update is called once per frame
	void Update()
	{
		if (miniPanel.activeSelf) {
			Appodeal.show (Appodeal.BANNER_BOTTOM);
		} else {
			Appodeal.hide (Appodeal.BANNER_BOTTOM);
		}
		if (transform.childCount != 0) 
		{
			ShowAd ();
			transform.GetChild (0).parent = null;
		}
	}
	public void ShowAd()
	{
		Invoke("ShowInterstitial", 1f);
	}
	public void ShowInterstitial()
	{		
		if (!Appodeal.isLoaded(Appodeal.INTERSTITIAL))
			return;
		Appodeal.show(Appodeal.INTERSTITIAL);
	}
	#endif
}