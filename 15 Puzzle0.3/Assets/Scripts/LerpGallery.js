﻿#pragma strict
private var inLerp : boolean = false;
private var pos : Vector3;
private var size : Vector3;
private var t1 : float;
private var t2 : float;
private var big : boolean = false;

private function OnMouseDown () {	
	Resize();
}
private function LerpPos(value : float, pos : Vector3, posEnd : Vector3) : IEnumerator {
	inLerp = true;
	while (t2 < 1.0){
		t2 += Time.deltaTime * 5.0;
		yield new WaitForSeconds (value);
		gameObject.transform.position = Vector3.Lerp (pos, posEnd, t2);
	}
	inLerp = false;
	yield  0;
}
private function LerpSca(value : float, size: Vector3, sizeEnd : Vector3) : IEnumerator {
	while (t1 < 1.0){		
		t1 += Time.deltaTime * 5.0;
		yield new WaitForSeconds (value);
		gameObject.transform.localScale = Vector3.Lerp (size, sizeEnd, t1);
	}
	GC.instance.backGallery.SetActive(!GC.instance.backGallery.activeSelf);
	GC.instance.backGallery.GetComponent(BackGalleryController).SetPicture(gameObject);
	yield  0;
}
public function Resize(){
	if(!inLerp){
		if (!big && GC.instance.GetZoom()){
			pos = gameObject.transform.position;
			size = gameObject.transform.localScale;
			t1 = 0;
			t2 = 0;
			StartCoroutine (LerpPos (0.01f, pos, new Vector3 (0f,0f,-2f)));
			StartCoroutine (LerpSca (0.01f, size, new Vector3 (2.7f,2.7f,1f)));
			GC.instance.SetZoom(false);
			big = !big;
		} else if(big && !GC.instance.GetZoom()) {
			t1 = 0;
			t2 = 0;
			StartCoroutine (LerpPos (0.01f, new Vector3 (0f,0f,-2f), pos));
			StartCoroutine (LerpSca (0.01f, new Vector3 (2.7f,2.7f,1f), size));
			GC.instance.SetZoom(true);
			big = !big;
		}
	}
}